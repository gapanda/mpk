﻿using System;
using ZeroFormatter;

namespace Mpk.Helper
{
    public static class PacketExtensions
    {
        public static byte[] ToByteArray<T>(this T packet)
        {
            if (packet == null)
            {
                throw new NullReferenceException();
            }
            return ZeroFormatterSerializer.Serialize(packet);
        }

        public static T ByteToFramePacket<T>(byte[] buffer)
        {
            return ZeroFormatterSerializer.Deserialize<T>(buffer);
        }
    }
}
