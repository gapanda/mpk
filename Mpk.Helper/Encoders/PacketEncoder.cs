﻿using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using System;
using ZeroFormatter;

namespace Mpk.Helper
{
    public class PacketEncoder : MessageToByteEncoder<Packet>
    {
        public override bool IsSharable => true;

        protected override void Encode(IChannelHandlerContext context, Packet message, IByteBuffer output)
        {
            IByteBuffer buffer = context.Allocator.Buffer();
            try
            {
                byte[] data = PacketExtensions.ToByteArray(message);

                buffer.WriteByte((byte)'F');
                buffer.WriteInt(data.Length);
                buffer.WriteBytes(data);
                output.WriteBytes(buffer);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                buffer.Release();
            }
        }
    }
}