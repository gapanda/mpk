﻿using System;
using ZeroFormatter;

namespace Mpk.Helper
{
    [ZeroFormattable]
    public class Packet
    {
        [Index(0)]
        public virtual string Type { get; set; }
        [Index(1)]
        public virtual byte[] PacketBuffer { get; set; }
    }
}