﻿using ZeroFormatter;

namespace Mpk.Helper
{
    [ZeroFormattable]
    public class ServerCommandPacket
    {
        [Index(0)] public virtual int Command { get; set; }
    }
}

