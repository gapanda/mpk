﻿using System;
using ZeroFormatter;

namespace Mpk.Helper
{
    [ZeroFormattable]
    public class DianaPacket
    {   //Метка времени
        [Index(0)]
        public virtual DateTime Time { get; set; }
        //Дополнительный канал
        [Index(1)]
        public virtual ushort OptionalIndex { get; set; }
        //КГР
        [Index(2)]
        public virtual ushort EDAIndex { get; set; }
        //Верхнее дыхание, ВДХ
        [Index(3)]
        public virtual ushort TRIndex { get; set; }
        //Нижнее дыхание, НДХ
        [Index(4)]
        public virtual ushort ARIndex { get; set; }
        //Плетизма, ПГ
        [Index(5)]
        public virtual ushort PLEIndex { get; set; }
        //Тремор, ТРМ
        [Index(6)]
        public virtual ushort TremorIndex { get; set; }
        //Артериальное давление, АД
        [Index(7)]
        public virtual ushort BVIndex { get; set; }
        //Тоническая составляющая КГР
        [Index(8)]
        public virtual ushort TEDAIndex { get; set; }
        //Абсолютное значение давления
        [Index(9)]
        public virtual ushort ABSBVIndex { get; set; }
    }
}
