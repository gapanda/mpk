﻿using System.Net;

namespace Mpk.Client
{
    public class ClientSettings
    {
        public static bool IsSsl
        {
            get
            {
                string ssl = ClientHelper.Configuration["ssl"];
                return !string.IsNullOrEmpty(ssl) && bool.Parse(ssl);
            }
        }

        public static IPAddress Host => IPAddress.Parse(ClientHelper.Configuration["host"]);

        public static int Port => int.Parse(ClientHelper.Configuration["port"]);

        public static int Size => int.Parse(ClientHelper.Configuration["size"]);

        public static bool UseLibuv
        {
            get
            {
                string libuv = ClientHelper.Configuration["libuv"];
                return !string.IsNullOrEmpty(libuv) && bool.Parse(libuv);
            }
        }
    }
}
