﻿using System;
using DotNetty.Transport.Channels;
using Mpk.Helper;

namespace Mpk.Client
{
    public class PacketClientHandler : SimpleChannelInboundHandler<Packet>
    {
        private readonly Action<Packet> receiveFrame;

        public PacketClientHandler(Action<Packet> receiveFrame)
        {
            this.receiveFrame = receiveFrame;
        }

        protected override void ChannelRead0(IChannelHandlerContext ctx, Packet msg)
        {
            receiveFrame?.Invoke(msg);
        }

        public override void ChannelReadComplete(IChannelHandlerContext context) => context.Flush();

        public override void ExceptionCaught(IChannelHandlerContext context, Exception exception)
        {
            Console.WriteLine(DateTime.Now.Millisecond);
            Console.WriteLine(exception.StackTrace);
            context.CloseAsync();
        }
    }
}
