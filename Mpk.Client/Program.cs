﻿using System;
using System.Net;
using System.Threading.Tasks;
using DotNetty.Handlers.Logging;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using Mpk.Helper;

namespace Mpk.Client
{
    class Program
    {
        static void Main()
        {
            var connection = new Connection();
            connection.ConnectAsync().Wait();
            connection.ReceiveFrame += SetFrame;

            Console.ReadLine();

            connection.CloseAsync().Wait();
        }

        private static void SetFrame(Packet packet)
        {
            Console.WriteLine($"Type - {packet.Type}, Size - {packet.PacketBuffer.Length}");
        }
    }
}