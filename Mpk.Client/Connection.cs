﻿using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using DotNetty.Handlers.Logging;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using Mpk.Helper;

namespace Mpk.Client
{
    public class Connection
    {
        public event Action<Packet> ReceiveFrame;

        private IChannel clientChannel;
        private IEventLoopGroup group;
        private PacketClientHandler framePacketClientHandler = null;

        public bool ConnectionStatus { get; set; } = false;

        public async Task ConnectAsync()
        {
            ClientHelper.SetConsoleLogger();

            group = new MultithreadEventLoopGroup();
            framePacketClientHandler = new PacketClientHandler(msg => ReceiveFrame?.Invoke(msg));

            try
            {
                var bootstrap = new Bootstrap();
                bootstrap
                    .Group(group)
                    .Channel<TcpSocketChannel>()
                    .Option(ChannelOption.TcpNodelay, true)
                    .Handler(new LoggingHandler(LogLevel.INFO))
                    .Handler(new ActionChannelInitializer<ISocketChannel>(channel =>
                    {
                        IChannelPipeline pipeline = channel.Pipeline;
                        pipeline.AddLast(new PacketEncoder());
                        pipeline.AddLast(new PacketDecoder());
                        pipeline.AddLast(framePacketClientHandler);
                    }));
                ConnectionStatus = true;
                clientChannel = await bootstrap.ConnectAsync(new IPEndPoint(ClientSettings.Host, ClientSettings.Port));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                ConnectionStatus = false;
            }
        }

        public async Task SendMessage(Packet packet)
        {
            if (packet == null)
            {
                return;
            }
            try
            {
                await clientChannel.WriteAndFlushAsync(packet);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public async Task SendServerCommand(ServerCommand command)
        {
            if (command == ServerCommand.StartRecording)
            {
                var serverCommandPacket = new ServerCommandPacket() { Command = (int)ServerCommand.StartRecording };
                await SendMessage(new Packet() { Type = "ServerCommand", PacketBuffer = serverCommandPacket.ToByteArray() });
            }
            else
            {
                var serverCommandPacket = new ServerCommandPacket() { Command = (int)ServerCommand.StopRecording };
                await SendMessage(new Packet() { Type = "ServerCommand", PacketBuffer = serverCommandPacket.ToByteArray() });
            }       
        }

        public async Task SendDianaPacket(DianaPacket packet)
        {
            await SendMessage(new Packet() { Type = "DianaPacket", PacketBuffer = packet.ToByteArray()});
        }

        public async Task CloseAsync()
        {
            await clientChannel?.CloseAsync();
            //await group.ShutdownGracefullyAsync();
            ConnectionStatus = false;
        }
    }
}
