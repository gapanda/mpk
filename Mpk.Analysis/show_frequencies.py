from argparse import ArgumentParser
import cv2
import numpy as np
import scipy.signal as signal
import scipy.fftpack as fftpack
from matplotlib import pyplot

def load_video(video_filename):
    cap=cv2.VideoCapture(video_filename)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    width, height = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    video_tensor=np.zeros((frame_count, height, width, 3),dtype='float')
    x=0
    while cap.isOpened():
        ret,frame=cap.read()
        if ret is True:
            video_tensor[x]=frame
            x+=1
        else:
            break
    return video_tensor,fps

def show_frequencies(vid_data, fps, bounds=None):
    averages = []

    if bounds:
        for x in range(1, vid_data.shape[0] - 1):
            averages.append(vid_data[x, bounds[2]:bounds[3], bounds[0]:bounds[1], :].sum())
    else:
        for x in range(1, vid_data.shape[0] - 1):
            averages.append(vid_data[x, :, :, :].sum())

    averages = averages - min(averages)

    charts_x = 1
    charts_y = 2
    pyplot.figure(figsize=(10, 5))
    pyplot.subplots_adjust(hspace=.5)

    pyplot.subplot(charts_y, charts_x, 1)
    pyplot.title("График среднего значения яркости пикселей")
    pyplot.xlabel("Кадры, уе")
    pyplot.ylabel("Яркость, уе")
    pyplot.plot(averages)

    freqs = fftpack.fftfreq(len(averages), d=1.0 / fps)
    fft = abs(fftpack.fft(averages))
    idx = np.argsort(freqs)

    pyplot.subplot(charts_y, charts_x, 2)
    pyplot.title("Быстрое преобразование Фурье")
    pyplot.xlabel("Частота, Гц")
    freqs = freqs[idx]
    fft = fft[idx]

    freqs = freqs[len(freqs) // 2 + 1:]
    fft = fft[len(fft) // 2 + 1:]
    max_fft = max(fft)
    x_pos = np.where(fft == max_fft)
    max_freq = freqs[x_pos]
    print(max_freq)
    print('BPM:', max_freq * 60)
    pyplot.plot(freqs, abs(fft))

    pyplot.show()

if __name__=="__main__":
    ap = ArgumentParser()    
    ap.add_argument("-v", "--video_name", required=False, help="path to video")
    args = vars(ap.parse_args())

    video,fps = load_video(args["video_name"])
    show_frequencies(video, fps)