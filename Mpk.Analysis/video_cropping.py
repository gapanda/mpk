# -*- coding: utf-8 -*-

from argparse import ArgumentParser
import dlib
import cv2
import numpy as np
import os

def video_cropping(video_name, output_path):
    output_filename = output_path + 'cropped_output.mp4'
    print("[INFO]Video name: ", args["video_name"])
    
    cap = cv2.VideoCapture(args["video_name"])
    fps = cap.get(cv2.CAP_PROP_FPS)
    print("[INFO]Video fps: ", fps)
    
    ret, frame = cap.read()
    height, width = frame.shape[:2]
    print("[INFO]Video size: ", height, width)
    
    hogFaceDetector = dlib.get_frontal_face_detector()
    faceRect = hogFaceDetector(frame, 0)[0]

    k = (faceRect.right() - faceRect.left()) // 4

    x1 = faceRect.left()-k
    if x1 < 0: 
        x1 = 0
    y1 = faceRect.top()-2*k
    if y1 < 0: 
        y1 = 0
    x2 = faceRect.right()+k
    if x2 > width: 
        y1 = width    
    y2 = faceRect.bottom()+k 
    if y2 > height:
        y2 = height

    out_x = x2 - x1
    out_y = y2 - y1
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_filename, fourcc, fps, (out_x, out_y))

    print("[INFO]Start cropping videofile...")
    while (cap.isOpened()):
        ret, frame = cap.read()
        if ret==True:
            #cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 0, 255), 2)
            cropped = frame[y1:y2, x1:x2]
            #cv2.imshow('frame_cropped', cropped)
            out.write(cropped)
            #cv2.imshow('frame',frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break
    print("[INFO]Finished!")

    cap.release()
    out.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    ap = ArgumentParser()    
    ap.add_argument("-v", "--video_name", required=False, help="path to video")
    ap.add_argument("-o", "--output_path", required=False, help="output path to video")
    args = vars(ap.parse_args())
    video_cropping(args["video_name"], args["output_path"])