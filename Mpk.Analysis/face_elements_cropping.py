# -*- coding: utf-8 -*-

from imutils import face_utils
from argparse import ArgumentParser
import dlib
import cv2
import numpy as np
import os

class Dot(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_tuple(self):
        return (self.x, self.y)

class Rectangle(object):
    def __init__(self, a, b):
        self.A = a
        self.B = b

    def get_tuple(self):
        return (self.width(), self.height())    

    def height(self):
        if self.A.y - self.B.y > 0:
            return self.A.y - self.B.y
        elif self.A.y - self.B.y < 0:
            return self.B.y - self.A.y
        else:
            raise Exception('Height equals zero!')

    def width(self):
        if self.A.x - self.B.x > 0:
            return self.A.x - self.B.x
        elif self.A.x - self.B.x < 0:
            return self.B.x - self.A.x
        else:
            raise Exception('Width equals zero!')

def count_left_cheek_box(left_cheek_dot, left_lip_dot):
    A = Dot(left_cheek_dot.x, left_cheek_dot.y)
    B = Dot(left_lip_dot.x, left_cheek_dot.y - (left_lip_dot.x - left_cheek_dot.x))
    return Rectangle(A, B)

def count_right_cheek_box(right_cheek_dot, right_lip_dot):
    A = Dot(right_cheek_dot.x, right_cheek_dot.y)
    B = Dot(right_lip_dot.x, right_cheek_dot.y - (right_cheek_dot.x - right_lip_dot.x))
    return Rectangle(A, B)

def count_forehead_box(left_eyebrow_dot, right_eyebrow_dot):
    if left_eyebrow_dot.y > right_eyebrow_dot.y:
        A = Dot(left_eyebrow_dot.x, left_eyebrow_dot.y)
        B = Dot(right_eyebrow_dot.x, right_eyebrow_dot.y + (right_eyebrow_dot.x - left_eyebrow_dot.x))
        return Rectangle(A, B)
    else:
        A = Dot(left_eyebrow_dot.x, left_eyebrow_dot.y)
        B = Dot(right_eyebrow_dot.x, left_eyebrow_dot.y + (right_eyebrow_dot.x - left_eyebrow_dot.x))
        return Rectangle(A, B)


def count_nose_box(left_eye_dot, right_eye_dot, nose_dot):
    A = Dot(left_eye_dot.x, nose_dot.y)
    B = Dot(right_eye_dot. x, right_eye_dot.y)
    return Rectangle(A, B)

def draw_dots(frame,
              left_cheek_dot, left_lip_dot, 
              right_cheek_dot, right_lip_dot, 
              left_eyebrow_dot, right_eyebrow_dot,
              left_eye_dot, right_eye_dot, nose_dot):
    cv2.circle(frame, left_cheek_dot.dot_tuple(), 3, (0, 0, 255), -1)
    cv2.circle(frame, left_lip_dot.dot_tuple(), 3, (0, 0, 255), -1)

    cv2.circle(frame, right_cheek_dot.dot_tuple(), 3, (0, 0, 255), -1)
    cv2.circle(frame, right_lip_dot.dot_tuple(), 3, (0, 0, 255), -1)        

    cv2.circle(frame, left_eye_dot.dot_tuple(), 3, (0, 0, 255), -1)
    cv2.circle(frame, right_eye_dot.dot_tuple(), 3, (0, 0, 255), -1)
    cv2.circle(frame, nose_dot.dot_tuple(), 3, (0, 0, 255), -1)

    cv2.circle(frame, left_eyebrow_dot.dot_tuple(), 3, (0, 0, 255), -1)
    cv2.circle(frame, right_eyebrow_dot.dot_tuple(), 3, (0, 0, 255), -1)

def draw_rectangles(frame, left_cheek_box, right_cheek_box, nose_box, forehead_box):
    cv2.rectangle(frame, left_cheek_box.A.get_tuple(), left_cheek_box.B.get_tuple(), (0, 0, 255), 2)
    cv2.rectangle(frame, right_cheek_box.A.get_tuple(), right_cheek_box.B.get_tuple(), (0, 0, 255), 2)
    cv2.rectangle(frame, nose_box.A.get_tuple(), nose_box.B.get_tuple(), (0, 0, 255), 2)
    cv2.rectangle(frame, forehead_box.A.get_tuple(), forehead_box.B.get_tuple(), (0, 0, 255), 2)

def face_elements_cropping(video_name, shape_predictor_path, output_path):
    if output_path is None:
        output_path = ""
    
    left_cheek_box_output_filename = output_path + "left_cheek_box_output.mp4"
    right_cheek_box_output_filename = output_path + "right_cheek_box_output.mp4"
    forehead_box_output_filename = output_path + "forehead_box_output.mp4"
    nose_box_output_filename = output_path +  "nose_box_output.mp4"

    cap = cv2.VideoCapture(video_name)
    fps = cap.get(cv2.CAP_PROP_FPS)
    print("[INFO] Video fps: ", fps)
    ret, frame = cap.read()
    hogFaceDetector = dlib.get_frontal_face_detector()
    faceRect = hogFaceDetector(frame, 0)[0]
    
    print("[PROCESS] Loading facial landmark predictor...")
    try:
        predictor = dlib.shape_predictor(args["shape_predictor"])
    except:
        raise Exception("[ERROR] Can't load predictor...")
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)  
    shape = predictor(gray, faceRect)
    shape = face_utils.shape_to_np(shape)

    left_cheek_dot = Dot(shape[4][0], shape[4][1])
    right_cheek_dot = Dot(shape[12][0], shape[12][1])
    nose_dot = Dot(shape[33][0], shape[33][1])
    right_lip_dot = Dot(shape[54][0], shape[54][1])
    left_lip_dot = Dot(shape[60][0], shape[60][1])

    left_eyebrow_dot = Dot(shape[23][0], shape[23][1])
    right_eyebrow_dot = Dot(shape[20][0], shape[20][1])

    left_eye_dot = Dot(shape[39][0], shape[39][1])
    right_eye_dot = Dot(shape[42][0], shape[42][1])

    left_cheek_box = count_left_cheek_box(left_cheek_dot, left_lip_dot)
    right_cheek_box = count_right_cheek_box(right_cheek_dot, right_lip_dot)
    forehead_box = count_forehead_box(left_eyebrow_dot, right_eyebrow_dot)
    nose_box = count_nose_box(left_eye_dot, right_eye_dot, nose_dot)
    
    print("[INFO]Start cropping videofile...")
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')

    left_cheek_box_out = cv2.VideoWriter(left_cheek_box_output_filename, fourcc, fps, left_cheek_box.get_tuple())
    right_cheek_box_out = cv2.VideoWriter(right_cheek_box_output_filename, fourcc, fps, right_cheek_box.get_tuple())
    forehead_box_out = cv2.VideoWriter(forehead_box_output_filename, fourcc, fps, forehead_box.get_tuple())
    nose_box_out = cv2.VideoWriter(nose_box_output_filename, fourcc, fps, nose_box.get_tuple())

    while (cap.isOpened()):
        # grab the frame from the threaded video stream
        ret, frame = cap.read()
        if ret == True:
            #draw_dots(frame, left_cheek_dot, left_lip_dot, right_cheek_dot, right_lip_dot, left_eyebrow_dot, right_eyebrow_dot, left_eye_dot, right_eye_dot, nose_dot)
            draw_rectangles(frame.copy(), left_cheek_box, right_cheek_box, nose_box, forehead_box)
            # show the frame
            cropped_left_cheek_box = frame[left_cheek_box.B.y:left_cheek_box.A.y, left_cheek_box.A.x:left_cheek_box.B.x]
            left_cheek_box_out.write(cropped_left_cheek_box)
            #cv2.imshow('frame_cropped', cropped_left_cheek_box)
            cropped_right_cheek_box = frame[right_cheek_box.B.y:right_cheek_box.A.y, right_cheek_box.B.x:right_cheek_box.A.x]
            right_cheek_box_out.write(cropped_right_cheek_box)
            #cv2.imshow('frame_cropped', cropped_right_cheek_box)
            cropped_forehead_box = frame[forehead_box.B.y:forehead_box.A.y, forehead_box.B.x:forehead_box.A.x]
            forehead_box_out.write(cropped_forehead_box)
            #cv2.imshow('frame_cropped', cropped_forehead_box)
            cropped_nose_box = frame[nose_box.B.y:nose_box.A.y, nose_box.A.x:nose_box.B.x]
            nose_box_out.write(cropped_nose_box)
            #cv2.imshow('frame_cropped', cropped_nose_box)
            #cv2.imshow("frame", frame)
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break
        else:
            break
            
    cap.release()
    left_cheek_box_out.release()
    right_cheek_box_out.release()
    forehead_box_out.release()
    nose_box_out.release()
    cv2.destroyAllWindows()
    print("[INFO]Finished!")

if __name__ == '__main__':
    ap = ArgumentParser()    
    ap.add_argument("-v", "--video_name", required=False,
                    help="path to video")    
    ap.add_argument("-p", "--shape_predictor", required=False,
                    default="shape_predictor_68_face_landmarks.dat",
                    help="path to facial landmark predictor")
    ap.add_argument("-o", "--output_path", required=False,
                    help="output path to video")  
    args = vars(ap.parse_args())

    face_elements_cropping(args["video_name"], args["shape_predictor"], args["output_path"])