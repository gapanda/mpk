% Generates all the results for the SIGGRAPH paper at:
% http://people.csail.mit.edu/mrub/vidmag
%
% Copyright (c) 2011-2012 Massachusetts Institute of Technology, 
% Quanta Research Cambridge, Inc.
%
% Authors: Hao-yu Wu, Michael Rubinstein, Eugene Shih
% License: Please refer to the LICENCE file
% Date: June 2012
%

clear;

dataDir = 'D:\pingv\Documents\MPK\Mpk.Analysis\Experiments\Vitalik\3';
resultsDir = 'D:\pingv\Documents\MPK\Mpk.Analysis\Experiments\Vitalik\3';

mkdir(resultsDir);
           
inFile = fullfile(dataDir,'cropped_output.mp4');
fprintf('Processing %s\n', inFile);
amplify_spatial_Gdown_temporal_ideal(inFile,resultsDir, 100, 6, ...
                     50/60, 2, 25, 1);