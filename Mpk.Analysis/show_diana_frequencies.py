import csv
from argparse import ArgumentParser
import numpy as np
import scipy.signal as signal
import scipy.fftpack as fftpack
from matplotlib import pyplot
import matplotlib.ticker as mticker
 
def csv_reader(file_obj):
    array = []
    reader = csv.reader(file_obj, delimiter=';')
    count = 0
    for row in reader:
        if count != 0:
            array.append(row[4])
        count += 1
    return array

def show_diana_frequencies(array, hz, bounds=None):
    charts_x = 1
    charts_y = 2
    pyplot.figure(figsize=(10, 5))
    pyplot.subplots_adjust(hspace=.5)

    pyplot.subplot(charts_y, charts_x, 1)
    pyplot.title("График плетизмографии")
    pyplot.xlabel("Отсчеты, уе")
    pyplot.ylabel("Значения датчика, уе")
    pyplot.yticks(np.arange(0, 5, step=100))
    pyplot.plot(array)

    freqs = fftpack.fftfreq(len(array), d=1.0 / hz)
    fft = abs(fftpack.fft(array))
    idx = np.argsort(freqs)

    pyplot.subplot(charts_y, charts_x, 2)
    pyplot.title("Быстрое преобразование Фурье")
    pyplot.xlabel("Частота, Гц")
    freqs = freqs[idx]
    fft = fft[idx]

    freqs = freqs[len(freqs) // 2 + 1:]
    fft = fft[len(fft) // 2 + 1:]
    max_fft = max(fft)
    x_pos = np.where(fft == max_fft)
    max_freq = freqs[x_pos]
    print(max_freq)
    print('BPM:', max_freq * 60)
    pyplot.plot(freqs, abs(fft))

    pyplot.show()

if __name__ == "__main__":
    ap = ArgumentParser()    
    ap.add_argument("-f", "--file_name", required=False, help="path to file")
    args = vars(ap.parse_args())
    pg_array = []
    with open(args["file_name"], "r") as f_obj:
        pg_array = csv_reader(f_obj)

    show_diana_frequencies(pg_array, 32)