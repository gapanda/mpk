﻿using System;
using System.IO;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;

namespace Mpk.ImageServer
{
    public class ImageChannel : IDisposable
    {
        private readonly int cameraId;
        private bool isStopped;
        private readonly string path;
        private FrameDecoder cameraDecoder;
        private FileStream streamSave;
        private CancellationToken token;

        public EventHandler<FrameReadyEventArgs> FrameHandler { get; set; }
        public Uri CameraAddress { get; private set; }

        public string Path { get; set; }
        public bool RecordingStatus { get; set; } = false;

        public ImageChannel(Uri cameraAddress, int cameraId, string path, CancellationToken token)
        {
            this.cameraId = cameraId;
            this.path = path;
            isStopped = false;
            this.token = token;
            CameraAddress = cameraAddress;
            cameraDecoder = new FrameDecoder();
        }

        public void Start()
        {
            cameraDecoder.ParseStream(CameraAddress, cameraId);

            cameraDecoder.FrameReady += FrameReady;

            Console.WriteLine($"[Info] Camera {cameraId} adress: {CameraAddress}.");

            while (!isStopped)
            {
                if (RecordingStatus & streamSave == null)
                {
                    Path = Directory.GetCurrentDirectory() + "\\1.mp4";
                    Console.WriteLine($"[Info] Path: {Path}.");
                    cameraDecoder.FrameReady += SaveFrame;
                    streamSave = new FileStream(Path, FileMode.Create);
                }
                if(RecordingStatus == false & streamSave != null)
                {
                    cameraDecoder.FrameReady -= SaveFrame;
                    streamSave?.Close();
                    streamSave?.Dispose();
                }

                if (token.IsCancellationRequested)
                { 
                    Stop();
                    token.ThrowIfCancellationRequested();
                }
            }
        }

        public void Stop()
        {
            isStopped = true;
            cameraDecoder.Stop();
            streamSave.Dispose();
        }

        public void Dispose()
        {
            Stop();
        }

        public void SaveFrame(object sender, FrameReadyEventArgs e)
        {
            streamSave?.Write(e.FrameBuffer);
        }

        private void FrameReady(object sender, FrameReadyEventArgs e)
        {
            FrameHandler?.Invoke(sender, e);
        }

        private void ProcessErrorFrame(object sender, ErrorEventArgs e)
        {
            throw new NotImplementedException();
        }

    }
}