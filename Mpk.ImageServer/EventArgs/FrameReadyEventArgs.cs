﻿using System;

namespace Mpk.ImageServer
{
    public class FrameReadyEventArgs : EventArgs
    {
        public int FrameId { get; private set; }
        public byte[] FrameBuffer { get; private set; }

        public FrameReadyEventArgs(int frameId, byte[] buffer)
        {
            FrameId = frameId;
            FrameBuffer = buffer;
        }
    }
}
