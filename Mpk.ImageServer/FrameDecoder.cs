﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Mpk.ImageServer
{
    public class FrameDecoder
    {
        private readonly byte[] jpegHeader = { 0xff, 0xd8 };
        private const int ChunkSize = 1024;
        private bool streamActive;
        private int lastTick;
        private int lastFrameRate;
        private int frameRate;

        public byte[] CurrentFrame { get; set; }
        public int CameraId { get; private set; }

        public int FrameRate
        {
            set { lastFrameRate = value; }
            get { return lastFrameRate; }
        }

        public event EventHandler<FrameReadyEventArgs> FrameReady;
        public event EventHandler<ErrorEventArgs> Error;

        public void ParseStream(Uri uri, int cameraId)
        {
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.BeginGetResponse(OnGetResponse, request);
            CameraId = cameraId;
        }

        public void Stop()
        {
            streamActive = false;
        }

        private void OnGetResponse(IAsyncResult asyncResult)
        {
            var imageBuffer = new byte[1024 * 1024];

            var req = (HttpWebRequest)asyncResult.AsyncState;

            try
            {
                var resp = (HttpWebResponse)req.EndGetResponse(asyncResult);

                var contentType = resp.Headers["Content-Type"];
                if (!string.IsNullOrEmpty(contentType) && !contentType.Contains("="))
                {
                    throw new Exception("Invalid content-type header. The camera is likely not returning a proper MJPEG stream.");
                }

                var boundary = resp.Headers["Content-Type"].Split('=')[1].Replace("\"", "");
                var boundaryBytes = Encoding.UTF8.GetBytes(boundary.StartsWith("--") ? boundary : "--" + boundary);

                using (var s = resp.GetResponseStream())
                using (var br = new BinaryReader(s))
                {
                    streamActive = true;

                    var buff = br.ReadBytes(ChunkSize);

                    while (streamActive)
                    {
                        var imageStart = buff.Find(jpegHeader);

                        if (imageStart != -1)
                        {
                            var size = buff.Length - imageStart;
                            Array.Copy(buff, imageStart, imageBuffer, 0, size);

                            while (true)
                            {
                                buff = br.ReadBytes(ChunkSize);

                                var imageEnd = buff.Find(boundaryBytes);
                                if (imageEnd != -1)
                                {
                                    Array.Copy(buff, 0, imageBuffer, size, imageEnd);
                                    size += imageEnd;

                                    var frame = new byte[size];
                                    Array.Copy(imageBuffer, 0, frame, 0, size);

                                    ProcessFrame(frame);

                                    Array.Copy(buff, imageEnd, buff, 0, buff.Length - imageEnd);
                                    var temp = br.ReadBytes(imageEnd);

                                    Array.Copy(temp, 0, buff, buff.Length - imageEnd, temp.Length);
                                    break;
                                }
                                Array.Copy(buff, 0, imageBuffer, size, buff.Length);
                                size += buff.Length;
                            }
                        }
                    }        
                }
            }
            catch (Exception ex)
            {
                Error?.Invoke(this, new ErrorEventArgs(ex.Message));
            }
        }

        private void ProcessFrame(byte[] frame)
        {
            FrameReady?.Invoke(this, new FrameReadyEventArgs(0, frame));
            CurrentFrame = frame;

            if (Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = Environment.TickCount;
            }
            frameRate++;
        }
    }
}
