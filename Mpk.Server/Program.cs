﻿using System;
using System.Threading.Tasks;

namespace Mpk.Server
{
    public class Program
    {
        static void Main()
        {
            var server = new Server();
            server.RunServerAsync().Wait();

            Console.ReadLine();

            server.CloseAsync().Wait();
        }
    }
}
