﻿using System;

namespace Mpk.Server
{
    public static class ServerSettings
    {
        public static bool IsSsl
        {
            get
            {
                string ssl = ServerHelper.Configuration["ssl"];
                return !string.IsNullOrEmpty(ssl) && bool.Parse(ssl);
            }
        }

        public static int Port => int.Parse(ServerHelper.Configuration["port"]);

        public static Uri CameraIp1 => new Uri(ServerHelper.Configuration["cameraIp1"]);

        public static Uri CameraIp2 => new Uri(ServerHelper.Configuration["cameraIp2"]);

        public static bool UseLibuv
        {
            get
            {
                string libuv = ServerHelper.Configuration["libuv"];
                return !string.IsNullOrEmpty(libuv) && bool.Parse(libuv);
            }
        }
    }
}