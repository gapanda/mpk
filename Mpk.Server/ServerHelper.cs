﻿using System;
using DotNetty.Common.Internal.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging.Console;

namespace Mpk.Server
{
    public static class ServerHelper
    {
        static ServerHelper()
        {
            Configuration = new ConfigurationBuilder()
            .SetBasePath(ProcessDirectory)
            .AddJsonFile("appsettings.json")
            .Build();
        }

        public static string ProcessDirectory
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory;
            }
        }

        public static IConfigurationRoot Configuration { get; }

        public static void SetConsoleLogger()
        {
            InternalLoggerFactory.DefaultFactory.AddProvider(new ConsoleLoggerProvider((s, level) => true, false));
        }
    }
}