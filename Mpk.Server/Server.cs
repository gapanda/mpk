﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotNetty.Handlers.Logging;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Groups;
using DotNetty.Transport.Channels.Sockets;
using Mpk.Helper;
using Mpk.ImageServer;

/* Comment for testing
 * http://192.168.0.101/axis-cgi/mjpg/video.cgi?fps=25
 * http://192.168.0.102/axis-cgi/mjpg/video.cgi?fps=25
 * http://212.162.177.75/axis-cgi/mjpg/video.cgi
 * http://88.53.197.250/axis-cgi/mjpg/video.cgi
 * http://217.197.157.7:7070/axis-cgi/mjpg/video.cgi
 */

namespace Mpk.Server
{
    public class Server
    {
        private IEventLoopGroup bossGroup = null;
        private IEventLoopGroup workerGroup = null;
        private IChannel boundChannel = null;

        private ImageChannel imageChannel;
        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private PacketServerHandler framePacketServerHandler = null;

        public event Action<ServerCommandPacket> ReceiveServerCommand;
        public event Action<DianaPacket> ReceiveDianaPacket;

        public async Task RunServerAsync()
        {
            ServerHelper.SetConsoleLogger();

            bossGroup = new MultithreadEventLoopGroup(1);
            workerGroup = new MultithreadEventLoopGroup();

            imageChannel = new ImageChannel(ServerSettings.CameraIp1, 1, "", cancellationTokenSource.Token);
            var imageChannelTask = new Task(() => imageChannel.Start());
            imageChannelTask.Start();
            imageChannel.FrameHandler += SendCurrentFrame;

            framePacketServerHandler = new PacketServerHandler(msg => ReceiveServerCommand?.Invoke(msg), 
                                                               msg => ReceiveDianaPacket?.Invoke(msg));

            ReceiveServerCommand += OnReceiveServerCommand;

            try
            {
                var bootstrap = new ServerBootstrap();
                bootstrap.Group(bossGroup, workerGroup);

                bootstrap.Channel<TcpServerSocketChannel>();

                bootstrap
                    .Option(ChannelOption.SoBacklog, 100)
                    .Channel<TcpServerSocketChannel>()
                    .Handler(new LoggingHandler(LogLevel.INFO))
                    .ChildHandler(new ActionChannelInitializer<ISocketChannel>(channel =>
                    {
                        IChannelPipeline pipeline = channel.Pipeline;
                        pipeline.AddLast(new PacketEncoder());
                        pipeline.AddLast(new PacketDecoder());
                        pipeline.AddLast(framePacketServerHandler);

                    }));

                boundChannel = await bootstrap.BindAsync(ServerSettings.Port);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public async Task SendMessage(Packet packet)
        {
            if (packet == null)
            {
                return;
            }
            try
            {
                var group = framePacketServerHandler.GetGroup;
                if (group != null & group?.Count != 0)
                {
                    await group.WriteAndFlushAsync(packet);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public async Task CloseAsync()
        {
            await boundChannel?.CloseAsync();
            await Task.WhenAll(
                bossGroup.ShutdownGracefullyAsync(TimeSpan.FromMilliseconds(100), TimeSpan.FromSeconds(1)),
                workerGroup.ShutdownGracefullyAsync(TimeSpan.FromMilliseconds(100), TimeSpan.FromSeconds(1)));
        }

        private void OnReceiveServerCommand(ServerCommandPacket packet)
        {
            if (packet.Command == (int)ServerCommand.StartRecording)
            {
                SetRecordingStatus(true);
            }
            if (packet.Command == (int)ServerCommand.StopRecording)
            {
                SetRecordingStatus(false);
            }
        }

        private void SetRecordingStatus(bool status)
        {
            imageChannel.RecordingStatus = status;
        }

        private async void SendCurrentFrame(object sender, FrameReadyEventArgs args)
        {
            try
            {
                var packet = new Packet() { Type = "FramePacket", PacketBuffer = args.FrameBuffer };
                await SendMessage(packet);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
