﻿using System;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Groups;
using Mpk.Helper;

namespace Mpk.Server
{
    public class PacketServerHandler : SimpleChannelInboundHandler<Packet>
    {
        private readonly Action<ServerCommandPacket> receivedCommandPacket;
        private readonly Action<DianaPacket> receivedDianaPacket;

        public PacketServerHandler(Action<ServerCommandPacket> receivedCommandPacket, 
                                   Action<DianaPacket> receivedDianaPacket)
        {
            this.receivedCommandPacket = receivedCommandPacket;
            this.receivedDianaPacket = receivedDianaPacket;
        }

        private static volatile IChannelGroup group;

        public IChannelGroup GetGroup { get { return group; } }

        public override bool IsSharable => true;

        public override void ChannelActive(IChannelHandlerContext context)
        {
            if (group == null)
            {
                lock (this)
                {
                    if (group == null)
                    {
                        group = new DefaultChannelGroup(context.Executor);
                    }
                }
            }
            group.Add(context.Channel);
        }

        public override void ChannelInactive(IChannelHandlerContext context)
        {
            group.Remove(context.Channel);
        }

        public override void ChannelReadComplete(IChannelHandlerContext context) => context.Flush();

        public override void ExceptionCaught(IChannelHandlerContext context, Exception exception)
        {
            Console.WriteLine("Exception: " + exception);
            context.CloseAsync();
        }

        protected override void ChannelRead0(IChannelHandlerContext ctx, Packet msg)
        {
            if (msg.Type == "ServerCommand")
            {
                var commandPacket = PacketExtensions.ByteToFramePacket<ServerCommandPacket>(msg.PacketBuffer);
                receivedCommandPacket?.Invoke(commandPacket);
            }
            if (msg.Type == "DianaPacket")
            {
                var dianaPacket = PacketExtensions.ByteToFramePacket<DianaPacket>(msg.PacketBuffer);
                receivedDianaPacket?.Invoke(dianaPacket);
            }
        }

    }
}