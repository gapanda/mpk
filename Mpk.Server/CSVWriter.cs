﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using Mpk.Helper;

namespace Mpk.Server
{
    public class CsvHelper
    {
        private string path;

        public CsvHelper(string path)
        {
            this.path = path;
            using (var streamWriter = new StreamWriter(new FileStream(path + "/Output.csv", FileMode.Create), Encoding.UTF8))
            using (var writer = new CsvWriter(streamWriter))
            {
                
                writer.WriteRecord(new { Time = "Метка времени",
                                         OptionalIndex = "Дополнительный канал",
                                         EDAIndex = "КГР",
                                         TRIndex = "ВДХ",
                                         ARIndex = "НДХ",
                                         PLEIndex = "ПГ",
                                         TremorIndex = "ТРМ",
                                         BVIndex = "АД",
                                         TEDAIndex = "Тоническая составляющая КГР",
                                         ABSBVIndex = "Абсолютное значение давления"
                });
            }
        }

        public void WriteDataToCsv(DianaPacket packet)
        {
            try
            {
                using (var streamWriter = new StreamWriter(GetWriteStream(path + '\\' + "Output.csv", 2000), Encoding.UTF8))
                using (var writer = new CsvWriter(streamWriter))
                {
                    writer.WriteRecord(packet);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private FileStream GetWriteStream(string path, int timeoutMs)
        {
            var time = Stopwatch.StartNew();
            while (time.ElapsedMilliseconds < timeoutMs)
            {
                try
                {
                    return new FileStream(path, FileMode.Append, FileAccess.Write);
                }
                catch (IOException e)
                {
                    // access error
                    if (e.HResult != -2147024864)
                        throw;
                }
            }

            throw new TimeoutException($"Failed to get a write handle to {path} within {timeoutMs} ms.");
        }
    }
}