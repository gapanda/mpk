﻿using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace Mpk.UI
{
    public static class BitmapHelper
    {
        public static BitmapImage ToImage(this byte[] array)
        {
            using (var ms = new MemoryStream(array))
            {
                try
                {
                    var image = new BitmapImage();
                    image.BeginInit();
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = ms;
                    image.EndInit();
                    return image;
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
