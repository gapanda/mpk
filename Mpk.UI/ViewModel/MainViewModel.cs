using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Mpk.Client;
using Mpk.Helper;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System.Windows.Threading;

namespace Mpk.UI.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {
            ConnectCommand = new RelayCommand(ConnectAsync);
            DisconnectCommand = new RelayCommand(DisconnectAsync);
            SetConnectionStatus(false);
            TimerString = "00:00:00";
            RecordingStatusString = "������ �����������";
            StartRecordingCommand = new RelayCommand(StartRecordingAsync);
            StopRecordingCommand = new RelayCommand(StopRecordingAsync);
        }

        private Connection connection;
        private bool recordingStatus = false;

        private BitmapImage image1;
        private string connectionStatus;
        private string timer;
        private string recStatus;
        private DateTime startRecDateTime;
        private DispatcherTimer dispatcherTimer;

        public BitmapImage FrameImage1
        {
            get { return image1; }
            set { Set(ref image1, value); }
        }

        public string ConnectionStatusString
        {
            get { return connectionStatus; }
            set { Set(ref connectionStatus, value); }
        }

        public string TimerString
        {
            get { return timer; }
            set { Set(ref timer, value); }
        }

        public string RecordingStatusString
        {
            get { return recStatus; }
            set { Set(ref recStatus, value); }
        }

        public ICommand ConnectCommand { get; private set; }
        public ICommand DisconnectCommand { get; private set; }
        public ICommand StartRecordingCommand { get; private set; }
        public ICommand StopRecordingCommand { get; private set; }

        public string ServerIp { get; set; } = "127.0.0.1";
        public int ServerPort { get; set; } = 1234;

        public async void ConnectAsync()
        {
            try
            {
                if (connection == null)
                {
                    connection = new Connection();
                    await connection.ConnectAsync();
                    SetConnectionStatus(connection.ConnectionStatus);
                    connection.ReceiveFrame += SetFrame;
                }
            }
            catch (Exception ex)
            {
                SetConnectionStatus(connection.ConnectionStatus);
                Console.WriteLine(ex.Message);
                connection = null;
            }
        }

        public async void DisconnectAsync()
        {
            try
            {
                if (connection != null)
                {
                    ConnectionStatusString = "����������...";
                    connection.ReceiveFrame -= SetFrame;
                    await connection.CloseAsync();
                    SetNullFrame();
                    SetConnectionStatus(connection.ConnectionStatus);
                }
            }
            catch (Exception ex)
            {
                SetConnectionStatus(connection.ConnectionStatus);
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection = null;
                StopRecordingAsync();
            }
        }

        private void SetFrame(Packet packet)
        {
            try
            {
                var image = packet.PacketBuffer.ToImage();
                image.Freeze();
                Dispatcher.CurrentDispatcher.Invoke(() => FrameImage1 = image);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void SetNullFrame()
        {
            try
            {
                BitmapImage image = new BitmapImage();
                Dispatcher.CurrentDispatcher.Invoke(() => FrameImage1 = image);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private async void StartRecordingAsync()
        {
            try
            {
                if (connection != null & recordingStatus == false)
                {
                    await connection.SendServerCommand(ServerCommand.StartRecording);
                    recordingStatus = true;

                    startRecDateTime = DateTime.Now;
                    dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                    dispatcherTimer.Tick += dispatcherTimer_Tick;
                    dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
                    dispatcherTimer.Start();

                    RecordingStatusString = "������";
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void StopRecordingAsync()
        {
            try
            {
                if (dispatcherTimer != null)
                {
                    dispatcherTimer.Stop();
                }

                if (connection != null & recordingStatus == true)
                {
                    await connection.SendServerCommand(ServerCommand.StopRecording);
                    recordingStatus = false;
                    RecordingStatusString = "�����������";
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error on Stop Recording:\n" + exc.Message, "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            DateTime Now = DateTime.Now;
            TimeSpan timeSpan = Now.Subtract(startRecDateTime);
            TimerString = String.Format("{0:D2}:{1:D2}:{2:D2}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
        }

        private void SetConnectionStatus(bool connectionStatus)
        {
            if (connectionStatus == true)
            {              
               ConnectionStatusString = "���������";
            }
            else
            {
               ConnectionStatusString = "��������";
            }
        }
    }
}